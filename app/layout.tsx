import { Metadata } from 'next'
import './globals.css'

const RootLayout = ({ children }: { children: React.ReactNode }) => {
    return (
        <html lang="en">
            <body>{children}</body>
        </html>
    )
}

export const metadata: Metadata = {
    title: 'Matthijs Kok',
    description: 'Personal Website',
    icons: {
        icon: '/favicon.svg',
    },
}

export default RootLayout
