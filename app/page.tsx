import styles from './styles.module.css'
import { GithubIcon, GitlabIcon, LinkedinIcon } from '../components/IconLink'

const IndexPage = () => {
    return (
        <main className={styles.main}>
            <div className={styles.bio}>
                <p>I like to build websites</p>
                <p>Find me here</p>
                <GitlabIcon />
                <GithubIcon />
                <LinkedinIcon />
            </div>
            <div className={styles.name}>Matthijs Kok</div>
        </main>
    )
}

export default IndexPage
